
BookMan
=======

![BookMan Screenshot](./.preview/bmn-01.png)


BookMan is bookmark manager: it provides a friendly UI to save & organize your bookmarks.

See the [&rarr; demo GIF (8 MB)](./.preview/bookman-demo-2810.gif)

#### Stack

- React
- Redux
- Redux-Saga
- Firebase
